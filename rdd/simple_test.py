
from pyspark import SparkContext, SparkConf

def main():
    # Configuration de Spark
    conf = SparkConf().setAppName("simpleTest").setMaster("local")
    sc = SparkContext(conf=conf)

    # Lire un fichier texte
    text_file = sc.textFile("text.txt")

    # Compter le nombre de lignes
    count = text_file.count()

    # Afficher le nombre de lignes
    print(f"Nombre total de lignes : {count}")

    # Arrêter le SparkContext
    sc.stop()

if __name__ == "__main__":
    main()
